# Timed Switch

1. Wire component interrupt to system mute/other toggle functions.
2. Set Delay
3. Arm system by typing "armed" into status textbox.
4. Disarm system by holding deactivate button for 5 seconds.  You can make this transparent and hide it on a UCI if you like.